package pl.kkosobudzki.rxjava.app;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;
import rx.Observable;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author Krzysztof Kosobudzki
 */
public class FoodExpandableListAdapter extends BaseExpandableListAdapter {
    private final HashMap<Types.Meal, FoodDiaryGroupUi> mMap = new HashMap<Types.Meal, FoodDiaryGroupUi>();
    private final LayoutInflater mLayoutInflater;
    private final DecimalFormat mDecimalFormat = new DecimalFormat("#.#");

    public FoodExpandableListAdapter(final Context context) {
        mLayoutInflater = LayoutInflater.from(context);

        initMap();
    }

    private void initMap() {
        for (Types.Meal meal : Types.Meal.values()) {
            final FoodDiaryGroupUi groupUi = new FoodDiaryGroupUi();
            groupUi.setMeal(meal);
            groupUi.setId(meal.ordinal());

            mMap.put(meal, groupUi);
        }
    }

    @Override
    public int getGroupCount() {
        return Types.Meal.values().length;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return mMap.get(Types.Meal.values()[groupPosition]).getItems().size();
    }

    @Override
    public FoodDiaryGroupUi getGroup(int groupPosition) {
        return mMap.get(Types.Meal.values()[groupPosition]);
    }

    @Override
    public FoodDiaryUi getChild(int groupPosition, int childPosition) {
        return mMap.get(Types.Meal.values()[groupPosition]).getItems().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return getGroup(groupPosition).getId();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return getChild(groupPosition, childPosition).getId();
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        View v;

        final FoodDiaryGroupUi groupUi = getGroup(groupPosition);

        if (convertView == null) {
            v = mLayoutInflater.inflate(R.layout.food_group, parent, false);

            final GroupViewHolder viewHolder = new GroupViewHolder();
            viewHolder.nameTextView = (TextView) v.findViewById(R.id.name_text_view);
            viewHolder.caloriesTextView = (TextView) v.findViewById(R.id.calories_text_view);

            v.setTag(viewHolder);

            Observable.from(groupUi.getItems())
                    .flatMap(item -> item.getCaloriesObservable())
                    .subscribe(item -> viewHolder.caloriesTextView.setText(mDecimalFormat.format(groupUi.recalculateCalories())));
        } else {
            v = convertView;
        }

        final GroupViewHolder groupViewHolder = (GroupViewHolder) v.getTag();
        groupViewHolder.nameTextView.setText(groupUi.getMeal().toString());

        return v;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        View v;

        if (convertView == null) {
            v = mLayoutInflater.inflate(R.layout.food_item, parent, false);

            final ChildViewHolder viewHolder = new ChildViewHolder();
            viewHolder.plusTextView = (TextView) v.findViewById(R.id.plus_text_view);
            viewHolder.minusTextView = (TextView) v.findViewById(R.id.minus_text_view);
            viewHolder.amountTextView = (TextView) v.findViewById(R.id.amount_text_view);
            viewHolder.deleteTextView = (TextView) v.findViewById(R.id.delete_text_view);
            viewHolder.caloriesTextView = (TextView) v.findViewById(R.id.calories_text_view);

            v.setTag(viewHolder);
        } else {
            v = convertView;
        }

        final FoodDiaryUi diaryUi = getChild(groupPosition, childPosition);
        final ChildViewHolder childViewHolder = (ChildViewHolder) v.getTag();

        diaryUi.getAmountObservable().subscribe(amount -> childViewHolder.amountTextView.setText(mDecimalFormat.format(amount)));
        diaryUi.getCaloriesObservable().subscribe(calories -> childViewHolder.caloriesTextView.setText(mDecimalFormat.format(calories)));

        childViewHolder.plusTextView.setOnClickListener(view -> diaryUi.addAmount());
        childViewHolder.minusTextView.setOnClickListener(view -> diaryUi.subAmount());
        childViewHolder.deleteTextView.setOnClickListener(view -> parent.removeView(v));

        return v;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    public List<FoodDiaryUi> getItems() {
        final List<FoodDiaryUi> allItems = new ArrayList<>();

        for (int i = 0; i < getGroupCount(); i++) {
            allItems.addAll(getGroup(i).getItems());
        }

        return allItems;
    }

    private class GroupViewHolder {
        TextView nameTextView;
        TextView caloriesTextView;
    }

    private class ChildViewHolder {
        TextView plusTextView;
        TextView minusTextView;
        TextView deleteTextView;
        TextView amountTextView;
        TextView caloriesTextView;
    }
}
