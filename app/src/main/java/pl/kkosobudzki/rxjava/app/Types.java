package pl.kkosobudzki.rxjava.app;

/**
 * @author Krzysztof Kosobudzki
 */
public class Types {
    public enum Unit {
        GRAM, KG, ML, SS, TS;
    }

    public enum Meal {
        BREAKFAST, LUNCH, SNACK, DINNER, SUPPER;
    }
}
