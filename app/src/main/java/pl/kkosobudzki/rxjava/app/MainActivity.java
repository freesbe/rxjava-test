package pl.kkosobudzki.rxjava.app;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.*;
import rx.Observable;
import rx.Subscriber;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.observables.MathObservable;

import java.text.DecimalFormat;
import java.util.List;

public class MainActivity extends Activity {
    private static final String TAG = "rxjavatest";
    private static final int TOTAL = 1945;

    private final DecimalFormat mDecimalFormat = new DecimalFormat("#");

    private TextView mTotalTextView;
    private TextView mLeftTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        mTotalTextView = (TextView) findViewById(R.id.total_text_view);
        mLeftTextView = (TextView) findViewById(R.id.left_text_view);

        rxtest();
        simplerRxtest();
        operatorTest();
        lambdaTest();
        lambdaTestWithMap();

        testFood();
    }

    private void rxtest() {
        Observable<String> myObservable = Observable.create(
            new Observable.OnSubscribe<String>() {
                @Override
                public void call(Subscriber<? super String> subscriber) {
                    subscriber.onNext("Hello");
                    subscriber.onCompleted();
                }
            }
        );

        Subscriber<String> mySubscriber = new Subscriber<String>() {
            @Override
            public void onCompleted() { }

            @Override
            public void onError(Throwable throwable) { }

            @Override
            public void onNext(String s) {
                Log.d(TAG, String.format("Subscriber s: %s", s));
            }
        };

        myObservable.subscribe(mySubscriber);
    }

    private void simplerRxtest() {
        Observable<String> myObservable = Observable.just("Hello shorter");
        Action1<String> myAction = new Action1<String>() {
            @Override
            public void call(String s) {
                Log.d(TAG, String.format("Action1 s: %s", s));
            }
        };

        myObservable.subscribe(myAction);
    }

    private void operatorTest() {
        Observable.just("Hello operator!")
                .map(new Func1<String, String>() {
                    @Override
                    public String call(String s) {
                        return String.format("%s from map", s);
                    }
                })
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String s) {
                        Log.d(TAG, String.format("Action1 s: %s", s));
                    }
                });
    }

    private void lambdaTest() {
        Observable.just("Hello world, what's up there?")
                .subscribe(s -> Log.d(TAG, String.format("From lambda: %s", s)));
    }

    private void lambdaTestWithMap() {
        Observable.just("What up there?")
                .map(s -> String.format("%s -> map", s))
                .subscribe(s -> Log.d(TAG, s));
    }

    private void testFood() {
        final Observable<String> foodObservable = Observable.just("2,1.4,GRAM,BREAKFAST,20;3,50,ML,BREAKFAST,13;4,123,GRAM,SUPPER,44");

        final Observable<List<FoodDiaryUi>> listObservable = foodObservable
                .map(s -> FoodDiaryUi.readFromString(s));

        final FoodExpandableListAdapter listAdapter = new FoodExpandableListAdapter(this);
        final ExpandableListView listView = (ExpandableListView) findViewById(R.id.expandable_list_view);
        listView.setAdapter(listAdapter);

        // grouped
        listObservable
                .flatMap(items -> Observable.from(items))
                .groupBy(foodDiary -> foodDiary.getMeal())
                .subscribe(group -> {
                    final FoodDiaryGroupUi groupUi = listAdapter.getGroup(group.getKey().ordinal());
                    groupUi.getItems().clear();

                    group.subscribe(item -> groupUi.getItems().add(item));
                });

        Observable.from(listAdapter.getItems())
                .flatMap(item -> item.getCaloriesObservable())
                .subscribe(e -> {
                    Log.d(TAG, String.format("main activity on item"));

                    double sum = 0;

                    for (FoodDiaryUi foodDiaryUi : listAdapter.getItems()) {
                        sum += foodDiaryUi.getCalories();
                    }

                    mTotalTextView.setText(mDecimalFormat.format(sum));
                    mLeftTextView.setText(mDecimalFormat.format(TOTAL - sum));
                });
//        // all
//        MathObservable.sumDouble(
//                listObservable
//                        .flatMap(items -> Observable.from(items).map(item -> item.getCalories()))
//        ).subscribe(sum -> {
//            mTotalTextView.setText(mDecimalFormat.format(sum));
//            mLeftTextView.setText(mDecimalFormat.format(TOTAL - sum));
//        });
    }
}
