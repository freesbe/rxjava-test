package pl.kkosobudzki.rxjava.app;

import android.text.TextUtils;
import rx.Observable;
import rx.subjects.PublishSubject;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Krzysztof Kosobudzki
 */
public class FoodDiaryUi {
    private double mAmount;
    private Types.Unit mUnit;
    private Types.Meal mMeal;
    private double mCaloriesPerHg;
    private long mId;

    private final PublishSubject<Double> mAmountSubject = PublishSubject.create();

    public FoodDiaryUi() {

    }

    public static final List<FoodDiaryUi> readFromString(String s) {
        final List<FoodDiaryUi> list = new ArrayList<FoodDiaryUi>();

        if (TextUtils.isEmpty(s)) {
            return list;
        }

        String[] food = s.split(";");

        for (String f : food) {
            String[] data = f.split(",");

            final FoodDiaryUi foodDiaryUi = new FoodDiaryUi();
            foodDiaryUi.setId(Long.parseLong(data[0]));
            foodDiaryUi.setAmount(Double.parseDouble(data[1]));
            foodDiaryUi.setUnit(Types.Unit.valueOf(data[2]));
            foodDiaryUi.setMeal(Types.Meal.valueOf(data[3]));
            foodDiaryUi.setCaloriesPerHg(Double.parseDouble(data[4]));

            list.add(foodDiaryUi);
        }

        return list;
    }

    public Observable<Double> getAmountObservable() {
        return mAmountSubject;
    }

    public Observable<Double> getCaloriesObservable() {
        return getAmountObservable().map(amount -> amount * mCaloriesPerHg / 100.0d * 1.43d);
    }

    public double getCalories() {
        return mAmount * mCaloriesPerHg / 100.0d * 1.43d;
    }

    public void setAmount(double amount) {
        mAmount = amount;

        mAmountSubject.onNext(mAmount);
    }

    public void addAmount() {
        mAmount += 5;

        mAmountSubject.onNext(mAmount);
    }

    public void subAmount() {
        if (mAmount >= 5) {
            mAmount -= 5;

            mAmountSubject.onNext(mAmount);
        }
    }

    public void setUnit(Types.Unit unit) {
        mUnit = unit;
    }

    public void setCaloriesPerHg(double caloriesPerHg) {
        mCaloriesPerHg = caloriesPerHg;
    }

    public void setMeal(Types.Meal meal) {
        mMeal = meal;
    }

    public void setId(long id) {
        mId = id;
    }

    public Types.Meal getMeal() {
        return mMeal;
    }

    public long getId() {
        return mId;
    }
}
