package pl.kkosobudzki.rxjava.app;

import rx.Observable;
import rx.observables.MathObservable;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Krzysztof Kosobudzki
 */
public class FoodDiaryGroupUi {
    private final List<FoodDiaryUi> mItems = new ArrayList<FoodDiaryUi>();

    private Types.Meal mMeal;
    private long mId;

    public FoodDiaryGroupUi() {

    }

    public void setItems(List<FoodDiaryUi> items) {
        mItems.clear();
        mItems.addAll(items);
    }

    public Observable<Double> getCaloriesObservable() {
        List<Observable<Double>> observables = new ArrayList<Observable<Double>>();

        for (FoodDiaryUi item : mItems) {
            observables.add(item.getCaloriesObservable());
        }

        return MathObservable.sumDouble(Observable.merge(observables));
    }

    public double recalculateCalories() {
        double sum = 0.0d;

        for (FoodDiaryUi item : mItems) {
            sum += item.getCalories();
        }

        return sum;
    }

    public void setMeal(Types.Meal meal) {
        mMeal = meal;
    }

    public void setId(long id) {
        mId = id;
    }

    public List<FoodDiaryUi> getItems() {
        return mItems;
    }

    public long getId() {
        return mId;
    }

    public Types.Meal getMeal() {
        return mMeal;
    }
}
